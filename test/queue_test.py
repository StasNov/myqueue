import unittest
from queue import Queue


class QueueTest(unittest.TestCase):
    def test_add(self):
        q1 = Queue()
        q2 = Queue()
        desired_result = [1, 2, 3, 4, 5, 6]
        for i in range(1, 4):
            q1.insert(i)
        for i in range(4, 7):
            q2.insert(i)
        result = [value for value in (q1+q2)]
        # self.assertEqual(desired_result, result)
        self.assertListEqual(desired_result, result)

    def test_get_item(self):
        q = Queue()
        for i in range(1, 3):
            q.insert(i)
        self.assertEqual(1, q[0])
        self.assertEqual(2, q[1])

    def test_get_item_slice(self):
        desired_result = [3, 4, 5]
        desired_result2= [2, 4]
        q = Queue()
        for i in range(1, 7):
            q.insert(i)
        result = [value for value in q[2:5]]
        result2 = [value for value in q[1:5:2]]
        # self.assertEqual(desired_result, result)
        self.assertListEqual(desired_result, result)
        self.assertListEqual(desired_result2, result2)

    def test_length(self):
        q = Queue()
        for i in range(1, 12):
            q.insert(i)
        self.assertEqual(11, q.length)
