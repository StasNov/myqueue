from element import Element


class Queue:
    def __init__(self):
        self.length = 0
        self.head = None

    def is_empty(self):
        return self.length == 0

    def insert(self, value):
        elem = Element(value)

        if self.head is None:
            self.head = elem
        else:
            last = self.head
            while last.next:
                last = last.next
            last.next = elem

        self.length += 1

    def remove(self):
        if self.length == 0:
            return
        value = self.head.value
        self.head = self.head.next
        self.length -= 1
        return value

    def __iter__(self):
        self.current = self.head
        return self

    def __next__(self):
        if self.current is None:
            raise StopIteration
        result = self.current
        self.current = self.current.next
        return result.value

    def __getitem__(self, item):
        if isinstance(item, slice):
            start = item.start
            stop = item.stop
            step = item.step

            if start is None:
                start = 0
            if stop is None or stop > self.length:
                stop = self.length
            if step is None:
                step = 1

            keys = [value for value in range(start, stop, step)]

            result = Queue()
            if start >= self.length or start >= stop:
                return result

            current = self.head
            it = 0
            while it < stop:
                if it in keys:
                    result.insert(current.value)
                current = current.next
                it += 1

            return result

        if item >= self.length:
            raise Exception("Incorrect index")
        current = self.head
        result = current.value
        it = 0
        while it < item:
            current = current.next
            result = current.value
            it += 1
        return result

    def __setitem__(self, key, value):
        if key >= self.length:
            raise Exception("Incorrect index")
        current = self.head
        for i in range(key):
            current = current.next
        current.value = value

    def __add__(self, other):
        result = Queue()
        for elem in self:
            result.insert(elem)
        for elem in other:
            result.insert(elem)
        return result

    def __contains__(self, item):
        for elem in self:
            if elem == item:
                return True
        return False