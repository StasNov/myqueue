from queue import Queue

if __name__ == '__main__':
    print("Creation and completion Que")
    myq = Queue()
    myq.insert(3)
    myq.insert(4)
    myq.insert(7)
    myq.insert(8)
    myq.insert(9)
    myq.insert(12)
    myq.insert(25)

    print("\nWork with 'iter' and 'next'")
    iterator = iter(myq)
    print(next(iterator))
    print(next(iterator))
    print(next(iterator))
    print(next(iterator))
    print(next(iterator))
    print(next(iterator))
    print(next(iterator))
    # print(next(iterator)) #StopIteration Exception
    print("Let's try with iter")
    for elem in myq:
        print(elem)

    print("\nWork with get [index]")
    print(myq[0])
    print(myq[1])
    print(myq[2])
    print(myq[3])
    print(myq[4])
    print(myq[5])
    print(myq[6])
    # print(myq[7]) #Exception

    print("\nWork with set [index]")
    print(myq[0])
    myq[0] = 325
    print(myq[0])
    print(myq[6])
    myq[6] = 12656
    print(myq[6])
    # myq[7] = 1111 #Exception

    print("\nWork with slice")
    for i in myq:
        print(i, end=";")
    print("\n[1:5]")
    newQ = myq[1:5]
    for i in newQ:
        print(i, end=";")
    print("\n[:3]")
    newQ = myq[:3]
    for i in newQ:
        print(i, end=";")
    print("\n[4:]")
    newQ = myq[4:]
    for i in newQ:
        print(i, end=";")
    print("\n[:]")
    newQ = myq[:]
    for i in newQ:
        print(i, end=";")
    print("\n[7:] Output - length of Queue")
    newQ = myq[7:]
    print(newQ.length)
    print("[5:2] Output - length of Queue")
    newQ = myq[5:2]
    print(newQ.length)
    print("Work with slice with step")
    print("[::2]")
    newQ = myq[::2]
    for i in newQ:
        print(i, end=";")
    print("\n[1:5:2]")
    newQ = myq[1:5:2]
    for i in newQ:
        print(i, end=";")

    print("\n\nWork with PLUS")
    numbersOne = Queue()
    numbersOne.insert(4)
    numbersOne.insert(8)
    numbersOne.insert(15)
    numbersOne.insert(16)
    numbersOne.insert(23)
    numbersOne.insert(42)
    print("numbersOne - ", end="")
    for i in numbersOne:
        print(i, end=";")
    numbersTwo = Queue()
    numbersTwo.insert(11)
    numbersTwo.insert(21)
    numbersTwo.insert(31)
    numbersTwo.insert(41)
    numbersTwo.insert(51)
    numbersTwo.insert(61)
    print("\nnumbersTwo - ", end="")
    for i in numbersTwo:
        print(i, end=";")
    numbersAll = numbersOne + numbersTwo
    print("\nnumbersAll - ", end="")
    for i in numbersAll:
        print(i, end=";")

    print("\n\nWork with IN")
    print("21 in numbersAll: ", end="")
    print(21 in numbersAll)
    print("8 in numbersAll: ", end="")
    print(8 in numbersAll)
    print("99 in numbersAll: ", end="")
    print(99 in numbersAll)